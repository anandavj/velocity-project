package com.example.mapping.controller;

import com.example.mapping.model.Student;
import com.example.mapping.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("/list")
    public List<Student> getAllStudent() {
        return studentService.findAllStudent();
    }

    @PostMapping("/")
    public void createStudent(@RequestBody Student student) throws Exception {
        studentService.saveStudent(student);
    }

    @PostMapping("/assign")
    public void assignStudent(
            @RequestParam("studentId")Long studentId,
            @RequestParam("subjectId")Long subjectId
    ) throws Exception{
        studentService.assignStudent(studentId,subjectId);
    }

    @GetMapping("/list/{id}")
    public Student getByid(@PathVariable Long id) {
        return studentService.findStudentById(id);
    }
}
