package com.example.mapping.controller;

import com.example.mapping.model.Student;
import com.example.mapping.model.Subject;
import com.example.mapping.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("subject")
public class SubjectController {
    @Autowired
    private SubjectService subjectService;

    @GetMapping("/list")
    public List<Subject> getAllSubject() {
        return subjectService.findAllSubject();
    }

    @PostMapping("/")
    public void createSubject(@RequestBody Subject subject) throws Exception {
        subjectService.saveSubject(subject);
    }
}
