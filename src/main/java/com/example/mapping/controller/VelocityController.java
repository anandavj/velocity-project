package com.example.mapping.controller;

import com.example.mapping.dto.Velocity;
import com.example.mapping.service.VelocityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("velocity")
public class VelocityController {
    @Autowired
    private VelocityService velocityService;

    @PostMapping("/")
    public void create(@RequestBody Velocity velocity) throws Exception{
        velocityService.createModel(velocity.getName(),velocity.getProperties());
    }
}
