package com.example.mapping.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "student_subject")
public class StudentSubject {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;
}
