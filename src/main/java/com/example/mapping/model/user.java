package com.example.mapping.model;
import javax.persistence.*;

@Entity
@Table(name = "user")
public class user {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    private String name;
    private String address;

    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return this.address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
}