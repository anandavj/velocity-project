package com.example.mapping;

import com.example.mapping.template.Field;
import com.example.mapping.template.FilePathWriter;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableConfigurationProperties
@EntityScan(basePackages = {"com.example.mapping.model"})
public class MappingApplication {

    private static ConfigurableApplicationContext applicationContext;

    static String modelPackageName = "com.example.mapping.model";

    public static void main(String[] args) throws IOException {
        SpringApplication.run(MappingApplication.class, args);
//        VelocityEngine velocityEngine = new VelocityEngine();
//        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
//        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
//        velocityEngine.init();
//
//        Template t = velocityEngine.getTemplate("vtemplates/model.vm");
//        VelocityContext ctx = new VelocityContext();
//
//        if(modelPackageName != null) {
//            ctx.put("packagename", modelPackageName);
//        }
//
//        List<Field> properties = new ArrayList<>();
//        properties.add(new Field("id", "Long"));
//        properties.add(new Field("firstName", "String"));
//        properties.add(new Field("lastName", "String"));
//        ctx.put("modelName", "User");
//        ctx.put("properties", properties);
//
////        Printing the velocity result
////        StringWriter writer = new StringWriter();
////        t.merge( ctx, writer );
////
////        System.out.println(writer.toString());
//
////        FilePathWriter filePathWriter = new FilePathWriter();
////        filePathWriter.setFilePath("model","User");
////        Writer writer = new FileWriter(new File(filePathWriter.getFilePath()));
////        t.merge( ctx, writer );
////        writer.flush();
////        writer.close();
//
//        Writer writer = new FileWriter(new File(new FilePathWriter("model","User").getFilePath()));
//        t.merge(ctx,writer);
//        writer.flush();
//        writer.close();
    }

}
