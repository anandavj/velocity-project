package com.example.mapping.dto;

import com.example.mapping.template.Field;

import java.util.List;

public class Velocity {
    private String name;
    private List<Field> properties;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Field> getProperties() {
        return properties;
    }

    public void setProperties(List<Field> properties) {
        this.properties = properties;
    }
}
