package com.example.mapping.service;

import com.example.mapping.template.Field;
import com.example.mapping.template.FilePathWriter;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

@Service
public class VelocityService {

    static String modelPackageName = "com.example.mapping.model";

    public void createModel(String modelName, List<Field> properties) throws Exception{
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();

        Template template = velocityEngine.getTemplate("vtemplates/model.vm");
        VelocityContext ctx = new VelocityContext();

        ctx.put("packagename",modelPackageName);
        List<Field> modelProperties = new ArrayList<Field>();
        modelProperties.addAll(properties);
        ctx.put("modelName", modelName);
        ctx.put("properties",modelProperties);

        Writer writer = new FileWriter(new File(new FilePathWriter("model",modelName).getFilePath()));
        template.merge(ctx,writer);
        writer.flush();
        writer.close();
    }
}
