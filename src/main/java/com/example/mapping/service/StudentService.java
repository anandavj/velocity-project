package com.example.mapping.service;

import com.example.mapping.model.Student;
import com.example.mapping.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private SubjectService subjectService;

    public List<Student> findAllStudent() {
        List<Student> students = studentRepository.findAll();
        System.out.println(students);
        return students;
    }

    public void saveStudent(Student student) throws Exception {
        try {
            studentRepository.save(student);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public Student findStudentById(long id) {
        return studentRepository.findById(id).orElse(null);
    }

    public void assignStudent(long studentId, long subjectId) throws Exception {
        try {
            Student student = findStudentById(studentId);
            student.getSubjects().add(subjectService.findSubjectById(subjectId));
            saveStudent(student);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }
}
