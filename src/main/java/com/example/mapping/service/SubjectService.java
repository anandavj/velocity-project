package com.example.mapping.service;

import com.example.mapping.model.Subject;
import com.example.mapping.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectService {
    @Autowired
    private SubjectRepository subjectRepository;

    public List<Subject> findAllSubject() {
        return subjectRepository.findAll();
    }

    public Subject findSubjectById(long id) {
        return subjectRepository.findById(id).orElse(null);
    }

    public void saveSubject(Subject subject) throws Exception {
        try{
            subjectRepository.save(subject);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }
}
