package com.example.mapping.template;

public class Field {
    private String fieldName;
    private String fieldType;

    public Field(String fieldName, String fieldType) {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getGetterAndSetterField() {
        return this.fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
    }
}
