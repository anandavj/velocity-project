package com.example.mapping.template;

public class FilePathWriter {

    private String filePath;

    public FilePathWriter(String fileType, String className) {
        this.filePath = "./src/main/java/com/example/mapping/"+fileType+"/"+className+".java";
    }

//    public void setFilePath(String fileType, String className) {
//        this.filePath = "./src/main/java/com/example/mapping/"+fileType+"/"+className+".java";
//    }

    public String getFilePath() {
        return filePath;
    }
}
