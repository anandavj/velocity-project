package com.cimbniaga.rmtoolsapp;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Set;

import org.assertj.core.internal.Predicates;
import org.junit.Test;
import static org.reflections.ReflectionUtils.*;
import org.reflections.ReflectionUtils;

import com.cimbniaga.rmtoolsapp.model.Lookup;

public class ReflectionTest {

    @Test
    public void testFields() throws Exception{
//		Reflections reflections = new Reflections("com.cimbniaga.rmtoolsapp.model");
//		Reflections reflections = new Reflections("com.cimbniaga.rmtoolsapp.model", new SubTypesScanner());

        Set<Field> fields=ReflectionUtils.getAllFields(Lookup.class);
        for (Field field : fields) {
            System.out.println("======================");
            System.out.println("Field name: "+field.getName());
            System.out.println("Return type: "+field.getGenericType());
            Annotation[] annotations=field.getDeclaredAnnotations();
            for (Annotation annotate : annotations) {
                Class<? extends Annotation> type = annotate.annotationType();
                System.out.println("Values of " + type.getSimpleName() + " {");

                for (Method method : type.getDeclaredMethods()) {
                    Object value = method.invoke(annotate, (Object[])null);
                    System.out.println("    " + method.getName() + ": " + value);
                }
                System.out.println("}");
            }
        }



    }

    @Test
    public void testAnnotations(){
//		Reflections reflections = new Reflections("com.cimbniaga.rmtoolsapp.model");
//		Reflections reflections = new Reflections("com.cimbniaga.rmtoolsapp.model", new SubTypesScanner());

        Set<Annotation> annotations=ReflectionUtils.getAllAnnotations(Lookup.class);

        for (Annotation annotation : annotations) {
            System.out.println(annotation.toString());
        }



    }

}
